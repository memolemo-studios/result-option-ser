import { Result } from "@rbxts/rust-classes";
import { t } from "@rbxts/t";

export namespace ResultSer {
	/** Serialized type for Result */
	export type Serialized<Ok = defined, Err = defined> =
		| {
				type: "Ok";
				value: Ok;
		  }
		| {
				type: "Err";
				value: Err;
		  };

	/** Typechecker for serialized Result */
	export const serializedCheck: t.check<Serialized> = t.union(
		t.strictInterface({
			type: t.literal("Ok"),
			value: t.any,
		}),
		t.strictInterface({
			type: t.literal("Err"),
			value: t.any,
		}),
	);

	/** Deserializes serialized Result */
	export function deserialize<S extends defined, F extends defined>(output: Serialized<S, F>): Result<S, F>;
	export function deserialize<S extends defined, F extends defined>(output: unknown): Result<defined, defined>;
	export function deserialize<S extends defined, F extends defined>(
		output: Serialized<S, F> | unknown,
	): Result<S, F> {
		assert(serializedCheck(output), "Invalid serialized 'Result' value");
		if (output.type === "Err") {
			return Result.err(output.value as F);
		}
		return Result.ok(output.value as S);
	}

	/** Serialized result */
	export function serialize<S, F>(value: Result<S, F>): Serialized<S, F> {
		if (value.isErr() === true) {
			return { type: "Err", value: value.unwrapErr() };
		}
		return { type: "Ok", value: value.unwrap() };
	}
}
