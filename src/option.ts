import { Option } from "@rbxts/rust-classes";
import { t } from "@rbxts/t";

export namespace OptionSer {
	/** Serialized type for Result */
	export type Serialized<T extends defined = defined> = { type: "None" } | { type: "Some"; value: T };

	/** Typechecker for serialized Result */
	export const serializedCheck = t.union(
		t.strictInterface({
			type: t.literal("None"),
		}),
		t.strictInterface({
			type: t.literal("Some"),
			value: t.any,
		}),
	);

	/** Deserializes serialized Option */
	export function deserialize<T extends defined>(output: Serialized<T>): Option<T>;
	export function deserialize(output: unknown): Option<defined>;
	export function deserialize<T>(output: unknown | Serialized<T>) {
		assert(serializedCheck(output), "Invalid serialized 'Option' value");
		if (output.type === "None") {
			return Option.none<T>();
		}
		return Option.some<T>(output.value as T);
	}

	/** Serializes option */
	export function serialize<T>(value: Option<T>): Serialized<T> {
		if (value.isNone() === true) {
			return { type: "None" };
		}
		return { type: "Some", value: value.unwrap() };
	}
}
